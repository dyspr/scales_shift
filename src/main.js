var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var steps = 256
var initSize = 0.25

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < steps; i++) {
    for (var j = 0; j < 10; j++) {
      push()
      translate((j - 5) * boardSize * initSize * 0.5, 0)
      fill(255 * abs(sin(i * 0.075 + frameCount * 0.05 + Math.PI * (j % 2))))
      noStroke()
      push()
      translate(windowWidth * 0.5 + tan(i * 0.005 + frameCount * 0.05) * boardSize * initSize * 0.005, windowHeight * 0.5 + (i - steps * 0.325) * 0.02 * boardSize * initSize - 1.325 * boardSize * initSize * sin(Math.PI * (j % 2) * 0.5))
      ellipse(0, 0, boardSize * initSize * (0.5 + 0.5 * sin(i * 0.05)))
      pop()
      pop()
    }
  }

  // masking
  fill(colors.light)
  noStroke()
  rect(windowWidth * 0.5, (windowHeight - boardSize) * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect(windowWidth * 0.5, windowHeight * 0.75 + boardSize * 0.25, windowWidth, (windowHeight - boardSize) * 0.5 + 2)
  rect((windowWidth - boardSize) * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
  rect(windowWidth * 0.75 + boardSize * 0.25, windowHeight * 0.5, (windowWidth - boardSize) * 0.5, windowHeight)
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
